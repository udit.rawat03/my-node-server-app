import mongoose from "mongoose";

// TODO: add uniquness and email validation for email
const schema = mongoose.Schema(
  {
    email: { type: String, required: true, lowercase: true },
    passwordHash: { type: String, required: true }
  },
  { timestamps: true }
);

schema.methods.isValidPassword = function isValidPassword(password) {
  return password === this.passwordHash;
};

export default mongoose.model("User", schema);
