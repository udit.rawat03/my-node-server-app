import express from "express";
import path from "path";
// import mongoose from "mongoose";
import auth from "./routes/auth";
import bodyParser from "body-parser";

const app = express();
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

// mongoose.connect("mongodb://localhost/test", {
//   useNewUrlParser: true,
//   useUnifiedTopology: true
// });

app.use("/api/auth", auth);

app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.listen(8081, () => console.log("Running server on localhost 8081"));
