import express from "express";
import User from "../models/User";

const router = express.Router();

router.post("/", (req, res) => {
  const { credentials } = req.body;
  console.log(req.body);
  let response = res;
  User.findOne({ email: credentials.email }).then(user => {
    if (user && user.isValidPassword(credentials.password)) {
      response.json({ sucess: true });
    } else {
      response.status(400).json({ errors: { global: "Invalid Credentials" } });
    }
  });
  response.send("User not found");
});

export default router;
